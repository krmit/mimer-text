# Mimer Text

A simple protocol for giving properties to texts so that they can be used for
style or interaction.

## Install

```
$ npm install @mimer/text
```

## API

## License

ICS, see LICENSE file

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.6.0** _2022-06-27_ First version of a complete rewrite

## Author

**©Magnus Kronnäs 2022 [magnus.kronnas.se](https://magnus.kronnas.se)**
