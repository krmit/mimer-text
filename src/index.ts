#!/usr/bin/env node

type List = "line" | "ordered" | "unordered";
type Mood = "good" | "bad";
type DataType = "number" | "string";
type Notability = "important" | "warning";
export type Answer = { [key: string]: string | number };

export interface TextData {
  data?: (TextData | string)[];
  headline?: 1 | 2 | 3 | 4 | 5 | 6;
  notability?: Notability;
  mood?: Mood;
  dataType?: DataType;
  line?: true;
  list?: List;
  question?: string;
  next?: string;
}

export class Text {
  _: TextData = {};
  data: (Text | string)[];

  constructor(msg?: string) {
    this.data = msg ? [msg] : [];
  }

  add(...msg: (Text | string)[]) {
    this.data = [...this.data, ...msg];
    return this;
  }

  append(msg: Text | string) {
    let result: Text | string;
    if (typeof msg === "string") {
      result = new Text(msg);
    } else {
      result = msg;
    }
    this.data.push(result);
    return result;
  }

  get important() {
    this._.notability = "important";
    return this;
  }

  get bad() {
    this._.mood = "bad";
    return this;
  }

  get good() {
    this._.mood = "good";
    return this;
  }

  list(param: List) {
    this._.list = param;
    return this;
  }

  get lines() {
    this._.list = "line";
    return this;
  }

  get ordered() {
    this._.list = "ordered";
    return this;
  }

  get unordered() {
    this._.list = "unordered";
    return this;
  }

  get number() {
    this._.dataType = "number";
    return this;
  }

  get string() {
    this._.dataType = "string";
    return this;
  }

  get line() {
    this._.line = true;
    return this;
  }

  get headline() {
    this._.headline = 1;
    return this;
  }

  // UI
  question(name: string) {
    this._.question = name;
    return this;
  }

  next(url: string) {
    this._.next = url;
    return this;
  }
}

export function TextToJson(msg: Text): TextData {
  const result: TextData = Object.assign({}, msg._);
  result.data = [];
  if (msg.data) {
    for (let i = 0; i < msg.data.length; i++) {
      let item = msg.data[i];
      if (typeof item === "string") {
        result.data[i] = item;
      } else {
        result.data[i] = TextToJson(item);
      }
    }
  }
  return result;
}
